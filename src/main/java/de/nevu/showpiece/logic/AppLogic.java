package de.nevu.showpiece.logic;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.inject.Inject;

import de.nevu.showpiece.data.DataAccess;



public class AppLogic {
	
	@Inject
	public AppLogic(DataAccess dao, SerializerService SerializerService) {
		this.dao = dao;
		this.serializer=SerializerService;
	}

	private DataAccess  dao;
	private SerializerService serializer;
	public Boolean doesUserExist(String name)
	{
		return dao.userExists(name);
	}
	
	public List<ToDo> getTodosFromUser(String name) throws Exception
	{
		return getUser(name).ToDos;
	}
	
	public User getUser(String name) throws Exception
	{
		return dao.GetUser(name);
	}

	public void addUser(String name) {
		dao.addNewUser(name);
	}
	
	public String serializeToString(Object o)
	{
		return serializer.SerializePOJOtoString(o);
	}

	public void addTodoToUser(String name, ToDo toDo) {
		try {
			dao.addNewToDo(name, toDo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void shutDown() {
		dao.close();
	}
	
	public void Maintaining() {
		System.out.println("Doing some maintainance work every minute!");
	}
	
}
