package de.nevu.showpiece.logic;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.mapping.Mapper;
import org.mongodb.morphia.query.Query;
import org.bson.types.ObjectId;

public class User {
	

	//this Constructor is mainly for Morphia
	public User()
	{
	}
	
	public User(String name)
	{
		this.name = name;
		ToDos = new ArrayList<ToDo>();
	}

	//for Morphia
	 @Id private ObjectId id;

	// Need this to make it possible to modify a User in the DB 
	public Query<User> queryToFindMe(Datastore datastore) {
	      return datastore.createQuery(User.class).field(Mapper.ID_KEY).equal(id);
	}
	 
	public String name;
	
	@Embedded
	public List<ToDo> ToDos;
}
