package de.nevu.showpiece.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.nevu.showpiece.logic.AppLogic;
import de.nevu.showpiece.logic.ToDo;

/**
 * Servlet implementation class Main
 */
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		AppLogic AL=(AppLogic) request.getServletContext().getAttribute("APPLOGIC");
	    
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String action=request.getParameter("action");

	    String UserName=request.getParameter("name");
	    
		if(action.equals("add_todo"))
		{
			AL.addTodoToUser(UserName,
					new ToDo(UserName, request.getParameter("caption"), 
							request.getParameter("description"), 1)
					);
			out.println("New ToDo added!<br>");
		}
		
		out.println("");
		out.println("<form action='Main'>");
		out.println("Caption:<input type='text' name='caption'><br>");
		out.println("Description:<input type='text' name='description'><br>");
		out.println("<input type='hidden' name='name' value='"+UserName+"'>");
		out.println("<input type='submit' name='action' value='add_todo' />");
		out.println("</form>");

	      if(AL.doesUserExist(UserName))
	      {
	    	  try {
	    		  
	    		  
				for(ToDo todo: AL.getTodosFromUser(UserName))
				  {
					  out.println(todo.caption+" : "+todo.description);
				  }
				
				//out.println(AL.serializeToString(AL.getUser(UserName)));
				

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }
	      else
	      {
	    	  out.println("Unknown User");
	    	  AL.addUser(UserName);
	      }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
