package de.nevu.showpiece.data;

import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.User;

public interface DataAccess {
	public void addNewUser(String name);
	
	public void addNewToDo(String name, ToDo newToDo) throws Exception;

	public Boolean userExists(String name);
	
	public User GetUser(String name) throws Exception;

	public void close();

}
