package de.nevu.showpiece.data;

import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.User;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.*;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;


public class RedisImpl implements DataAccess {

	RedisClient redisClient;
	StatefulRedisConnection<String, String> connection;
	RedisCommands<String, String> syncCommands;
	
	ObjectMapper JSONmapper;
	
	public RedisImpl() {
		redisClient = RedisClient.create("redis://localhost:6379/0");
		connection = redisClient.connect();
		syncCommands = connection.sync();
		
		JSONmapper = new ObjectMapper();
		
	}
	
	@Override
	public void addNewUser(String name) {
		try {
			syncCommands.set(name, JSONmapper.writeValueAsString(new User(name)));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addNewToDo(String name, ToDo newToDo) throws Exception {
		User user = GetUser(name);
		user.ToDos.add(newToDo);
		
		try {
			syncCommands.set(name, JSONmapper.writeValueAsString(user));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Boolean userExists(String name) {
		try {
			GetUser(name);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public User GetUser(String name) throws Exception {
		String userAsJSON=syncCommands.get(name);
		if(userAsJSON!=null)
		{
			return JSONmapper.readValue(userAsJSON, User.class);
		}
		throw new Exception();
	}

	@Override
	public void close() {
		connection.close();
		redisClient.shutdown();

	}

}
