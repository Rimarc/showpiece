package de.nevu.showpiece.data;

import java.util.ArrayList;
import java.util.List;

import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.User;

public class VolatileMemmoryImpl implements DataAccess {

	public VolatileMemmoryImpl() {
		Users=new ArrayList<User>();
		Users.add(new User("Mark"));
		try {
			addNewToDo("Mark", new ToDo("Mark", "Lernen", "Lerne mehr Java!", 10));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	List<User> Users;
	
	public void addNewUser(String Name) {
		Users.add(new User(Name));
	}

	public Boolean userExists(String name) {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				return true; 
			}
		}
		return false;
	}


	public void addNewToDo(String name, ToDo newToDo) throws Exception {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				user.ToDos.add(newToDo);
				return; 
			}
		}
		throw new Exception("Wanted to add new Todo to not existing User");
	}

	public User GetUser(String name) throws Exception {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				return user; 
			}
		}
		throw new Exception("Wanted to get User that doesnt exist");
	}

	@Override
	public void close() {
		return;
		
	}

}
