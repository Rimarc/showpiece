package de.nevu.showpiece.data;

import com.mongodb.MongoClient;
import java.util.ArrayList;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.UpdateOperations;

import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.User;

public class MongoDBImpl implements DataAccess {

	public MongoDBImpl() {
		this.mongoClient = new MongoClient();
		
		
		Morphia morphia= new Morphia();
		morphia.map(User.class).map(ToDo.class);
		datastore= morphia.createDatastore(mongoClient, "Users");
	}

	MongoClient mongoClient;
	Datastore datastore;
	
	public void addNewUser(String name) {
		datastore.save(new User(name));

	}

	public void addNewToDo(String name, ToDo newToDo) throws Exception {
		User user = GetUser(name);
		user.ToDos.add(newToDo);
		UpdateOperations<User> ops = datastore.createUpdateOperations(User.class).set("ToDos", user.ToDos);
	    datastore.update(user.queryToFindMe(datastore), ops);
	}

	public Boolean userExists(String name) {
		try {
			GetUser(name);
		} catch (Exception e) {
			return false;
		}
		return true;
		
	}

	public User GetUser(String name) throws Exception {
		User user=datastore.find(User.class,"name =", name).get();
		if(user==null)
			throw new Exception("Couldnt get User "+name);
		//when the Arrylist was empty when we createt the User, the Todolist doesnt get initialized with an list....
		if(user.ToDos==null)
		{
			user.ToDos=new ArrayList<ToDo>();
			System.out.println("We got a null Todolist...");
		}
		return user;
	}

	@Override
	public void close() {
		mongoClient.close();
		
	}

}
